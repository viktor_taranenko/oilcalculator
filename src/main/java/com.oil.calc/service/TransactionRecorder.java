package com.oil.calc.service;

import com.oil.calc.model.TransactionEntity;
import com.oil.calc.model.TransactionOperation;

import java.time.Instant;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TransactionRecorder {
    private static TransactionRecorder instance = new TransactionRecorder();
    private List<TransactionEntity> transactions = Collections.synchronizedList(new LinkedList<>());

    private TransactionRecorder() {
    }

    public static TransactionRecorder getInstance() {
        return instance;
    }

    public List<TransactionEntity> getTransactions() {
        return transactions;
    }

    public void recordTransaction(TransactionOperation operation, int quantity, double price, String oilId) {
        transactions.add(new TransactionEntity(Instant.now(), quantity, operation, price, oilId));
    }

}
