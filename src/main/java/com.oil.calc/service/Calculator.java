package com.oil.calc.service;

import com.oil.calc.model.OilEntity;
import com.oil.calc.model.TransactionEntity;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Calculator {

    public static double calculateRevenueYield(double price, OilEntity oilEntity) {
        return calculateOilRevenue(oilEntity) / price;
    }

    public static double calculatePriceEarningsRatio(double price, OilEntity oilEntity) {
        return price / calculateOilRevenue(oilEntity);
    }

    public static double calculateVolumeWeightedOilPrice(List<TransactionEntity> transactions) {
        List<TransactionEntity> lastTransactions = transactions.stream()
                .filter(transaction -> transaction.getTimestamp().isAfter(Instant.now().minus(30, ChronoUnit.MINUTES)))
                .collect(Collectors.toList());

        double sumPriceXQuantity = lastTransactions.stream()
                .mapToDouble(transaction -> transaction.getPrice() * transaction.getQuantity())
                .sum();
        double sumQuantity = lastTransactions.stream()
                .mapToDouble(TransactionEntity::getQuantity)
                .sum();

        return sumPriceXQuantity / sumQuantity;
    }

    private static double calculateOilRevenue(OilEntity oilEntity) {
        switch (oilEntity.getType()) {
            case STANDARD:
                return Double.valueOf(oilEntity.getFixedRevenue());
            case PREMIUM:
                return oilEntity.getVariableRevenue() * oilEntity.getBarrelValue();
            default:
                throw new IllegalArgumentException();
        }
    }

    public static double calculateInventoryIndex(List<TransactionEntity> transactions) {
        Map<String, List<TransactionEntity>> groupedByOilId =
                transactions
                        .stream()
                        .collect(Collectors.groupingBy(TransactionEntity::getOilId));

        Set<Double> totalTransactionPricesGroupedByOilId = groupedByOilId.values()
                .stream()
                .map(transactionEntities ->
                        transactionEntities.stream()
                                .mapToDouble(TransactionEntity::getPrice)
                                .sum())
                .collect(Collectors.toSet());

        double composition = 1d;
        for (Double aDouble : totalTransactionPricesGroupedByOilId) {
            composition *= aDouble;
        }
        return Math.pow(composition, 1d / totalTransactionPricesGroupedByOilId.size());
    }

}
