package com.oil.calc.model;

public enum OilType {
    STANDARD, PREMIUM
}
