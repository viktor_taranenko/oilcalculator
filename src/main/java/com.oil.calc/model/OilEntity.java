package com.oil.calc.model;

public class OilEntity {

    private String id;
    private OilType type;
    private Integer fixedRevenue;
    private Double variableRevenue;
    private Integer barrelValue;

    public OilEntity(String id, OilType type, Integer fixedRevenue, Double variableRevenue, Integer barrelValue) {
        this.id = id;
        this.type = type;
        this.fixedRevenue = fixedRevenue;
        this.variableRevenue = variableRevenue;
        this.barrelValue = barrelValue;
    }

    public String getId() {
        return id;
    }

    public OilType getType() {
        return type;
    }

    public Integer getFixedRevenue() {
        return fixedRevenue;
    }

    public Double getVariableRevenue() {
        return variableRevenue;
    }

    public Integer getBarrelValue() {
        return barrelValue;
    }

}
