package com.oil.calc.model;

import java.time.Instant;

public class TransactionEntity {
    Instant timestamp;
    Integer quantity;
    TransactionOperation transactionOperation;
    Double price;
    String oilId;

    public TransactionEntity(Instant timestamp, Integer quantity, TransactionOperation transactionOperation, Double price, String oilId) {
        this.timestamp = timestamp;
        this.quantity = quantity;
        this.transactionOperation = transactionOperation;
        this.price = price;
        this.oilId = oilId;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public TransactionOperation getTransactionOperation() {
        return transactionOperation;
    }

    public Double getPrice() {
        return price;
    }

    public String getOilId() {
        return oilId;
    }
}
