package com.oil.calc.model;

public enum TransactionOperation {
    BUY, SELL
}
