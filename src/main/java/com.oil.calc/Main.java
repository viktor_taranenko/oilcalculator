package com.oil.calc;

import com.oil.calc.model.OilEntity;
import com.oil.calc.model.OilType;
import com.oil.calc.model.TransactionOperation;
import com.oil.calc.service.Calculator;
import com.oil.calc.service.TransactionRecorder;

import java.util.*;

class Main {

    private static List<OilEntity> availableOils = new LinkedList<>();
    private static TransactionRecorder transactionRecorder = TransactionRecorder.getInstance();

    static {
        availableOils.add(new OilEntity("AAC", OilType.STANDARD, 1, null, 42));
        availableOils.add(new OilEntity("REW", OilType.STANDARD, 7, null, 47));
        availableOils.add(new OilEntity("BWO", OilType.STANDARD, 17, null, 61));
        availableOils.add(new OilEntity("TIM", OilType.PREMIUM, 5, 0.07d, 111));
        availableOils.add(new OilEntity("QFC", OilType.STANDARD, 22, null, 123));
    }

    public static void main(String[] args) {
        Random random = new Random();

        // 1.a)
        availableOils.forEach(oilEntity -> {
                    double price = random.nextDouble() * 100;
                    double revenueYield = Calculator.calculateRevenueYield(price, oilEntity);
                    System.out.printf("Revenue yield for oil %s: %.3f%n", oilEntity.getId(), revenueYield);
                });

        // 1.b)
        availableOils.forEach(oilEntity -> {
                    double price = random.nextDouble() * 100;
                    double priceEarningsRatio = Calculator.calculatePriceEarningsRatio(price, oilEntity);
                    System.out.printf("Price-Earnings Ratio oil %s: %.3f%n", oilEntity.getId(), priceEarningsRatio);

                });

        // 1.c)
        for (int i = 0; i < 100; i++) {
            TransactionOperation randomOperation = random.nextBoolean() ? TransactionOperation.BUY : TransactionOperation.SELL;
            int randomQuantity = random.nextInt(100);
            double randomPrice = random.nextDouble() * 100;
            String randomOilId = availableOils.stream()
                    .map(OilEntity::getId)
                    .skip(random.nextInt(availableOils.size()))
                    .findFirst()
                    .get();
            transactionRecorder.recordTransaction(randomOperation, randomQuantity, randomPrice, randomOilId);
        }

        // 1.d)
        double volumeWeightedOilPrice = Calculator.calculateVolumeWeightedOilPrice(transactionRecorder.getTransactions());
        System.out.printf("Volume Weighted Oil Price based on past 30 minutes transactions: %.3f%n", volumeWeightedOilPrice);

        // 2.
        // In this task it's not clear where "prices for all the types" should be taken from.
        // So I'll calculate a geometric mean of total transaction prices aggregated by oil type.
        // I hope that's what you've meant
        double inventoryIndex = Calculator.calculateInventoryIndex(transactionRecorder.getTransactions());
        System.out.printf("Inventory index: %.3f%n", inventoryIndex);

    }
}