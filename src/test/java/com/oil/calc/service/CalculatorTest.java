package com.oil.calc.service;


import com.oil.calc.model.OilEntity;
import com.oil.calc.model.OilType;
import com.oil.calc.model.TransactionEntity;
import com.oil.calc.model.TransactionOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

class CalculatorTest {

    private Random random = new Random();

    @Test
    void calculateRevenueYieldForStandardOil() {
        OilEntity standardOil = new OilEntity("STD", OilType.STANDARD, 42, random.nextDouble(), random.nextInt());
        double revenueYield = Calculator.calculateRevenueYield(100, standardOil);

        Assertions.assertEquals(0.42d, revenueYield);
    }

    @Test
    void calculateRevenueYieldForPremiumOil() {
        OilEntity premiumOil = new OilEntity("PRM", OilType.PREMIUM, random.nextInt(), 0.1d, 50);
        double revenueYield = Calculator.calculateRevenueYield(100, premiumOil);

        Assertions.assertEquals(0.05d, revenueYield);
    }

    @Test
    void calculatePriceEarningsRatioForStandardOil() {
        OilEntity standardOil = new OilEntity("STD", OilType.STANDARD, 42, random.nextDouble(), random.nextInt());
        double priceEarningsRatio = Calculator.calculatePriceEarningsRatio(420, standardOil);

        Assertions.assertEquals(10d, priceEarningsRatio);
    }

    @Test
    void calculatePriceEarningsRatioForPremiumOil() {
        OilEntity premiumOil = new OilEntity("PRM", OilType.PREMIUM, random.nextInt(), 0.1d, 50);
        double priceEarningsRatio = Calculator.calculatePriceEarningsRatio(420, premiumOil);

        Assertions.assertEquals(84d, priceEarningsRatio);
    }

    @Test
    void calculateVolumeWeightedOilPrice() {
        TransactionEntity obsoleteBuyTransaction =
                new TransactionEntity(Instant.MIN, random.nextInt(), TransactionOperation.BUY, random.nextDouble(), "OOB");
        TransactionEntity obsoleteSellTransaction =
                new TransactionEntity(Instant.MIN, random.nextInt(), TransactionOperation.SELL, random.nextDouble(), "OOB");
        TransactionEntity freshBuyTransaction =
                new TransactionEntity(Instant.now(), 20, TransactionOperation.BUY, 20d, "FSH");
        TransactionEntity freshSellTransaction =
                new TransactionEntity(Instant.now(), 30, TransactionOperation.SELL, 15d, "FSH");

        List<TransactionEntity> transactions = Arrays.asList(
                obsoleteBuyTransaction,
                obsoleteSellTransaction,
                freshBuyTransaction,
                freshSellTransaction);

        double volumeWeightedOilPrice = Calculator.calculateVolumeWeightedOilPrice(transactions);

        Assertions.assertEquals(17d, volumeWeightedOilPrice);
    }

    @Test
    void calculateInventoryIndex() {
        LinkedList<TransactionEntity> transactions = new LinkedList<>();

        transactions.add(new TransactionEntity(Instant.MIN, 10, TransactionOperation.BUY, 10d, "OOB"));
        transactions.add(new TransactionEntity(Instant.MIN, 10, TransactionOperation.SELL, 10d, "OOB"));
        transactions.add(new TransactionEntity(Instant.now(), 10, TransactionOperation.SELL, 10d, "FSH"));
        transactions.add(new TransactionEntity(Instant.now(), 10, TransactionOperation.SELL, 10d, "FSH"));

        double inventoryIndex = Calculator.calculateInventoryIndex(transactions);

        Assertions.assertEquals(20d, inventoryIndex);
    }
}