package com.oil.calc.service;

import com.oil.calc.model.TransactionOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class TransactionRecorderTest {

    @Test
    void record100KTransactions() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        int expectedRecordsCount = 100_000;
        for (int i = 0; i < expectedRecordsCount; i++) {
            executorService.execute(() -> TransactionRecorder.getInstance().recordTransaction(TransactionOperation.SELL, 0, 0d, "DDOS"));
        }
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);

        Assertions.assertEquals(expectedRecordsCount, TransactionRecorder.getInstance().getTransactions().size());
    }
}